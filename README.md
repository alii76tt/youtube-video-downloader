# Youtube Video Downloader
YouTube Video Downloader is a simple Python Web application built with Django.

[![Heroku](https://heroku-badge.herokuapp.com/?app=heroku-badge)](https://youtube-video-download-yt.herokuapp.com/)

## Screenshots
![1](https://user-images.githubusercontent.com/85459074/121768590-9366f400-cb67-11eb-939e-1803d29bde13.png)
![2](https://user-images.githubusercontent.com/85459074/121768598-a083e300-cb67-11eb-9995-2da3cefad774.png)
# Requirements
This App Uses Python==3.9.5, Django==3.2.3, papy==1.0.8.

# Installation

```sh
$ git clone https://github.com/alii76tt/youtube-video-downloader
$ cd youtube-video-downloader
$ pip install -r requirements.txt
$ python manage.py runserver
```
